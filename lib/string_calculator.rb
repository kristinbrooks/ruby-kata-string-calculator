class StringCalculator
  def add(string_to_add)
    return 0 if string_to_add == ''
    string_to_add.split(',').map(&:to_i).reduce(:+)
  end
end