require 'rspec'
require_relative '../lib/string_calculator'

describe StringCalculator do
  describe 'when given an empty string' do
    it 'returns 0' do
      result = described_class.new.add('')
      expect(result).to eq(0)
    end
  end

  describe 'when given a single number' do
    it 'returns the number' do
      result = described_class.new.add('1')
      expect(result).to eq(1)
      result = described_class.new.add('2')
      expect(result).to eq(2)
    end
  end

  describe 'when given two numbers' do
    it 'returns their sum' do
      result = described_class.new.add('1,2')
      expect(result).to eq(3)
    end
  end

  describe 'when given multiple numbers' do
    it 'returns their sum' do
      result = described_class.new.add('1,1')
      expect(result).to eq(2)
      result = described_class.new.add('1,1,2')
      expect(result).to eq(4)
      result = described_class.new.add('1,1,2,3')
      expect(result).to eq(7)
      result = described_class.new.add('1,1,2,3,5')
      expect(result).to eq(12)
    end
  end
end

